# -*- coding: utf-8 -*-
"""
Created on Fri Aug 21 19:10:59 2020

@author: Rajat
"""

#In this case, the odeint numerical solver was used to solve the nonlinear ODEs.
#We plot the time series in this case as well.

import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Model parameters and initial conditions
sigma, beta, E, Q, omega, gamma, phi, C, D = 0.5, 0.9, 1.0, 1.0, 0.8, 0.4, 0.1, 0.5, 1.0
x0, y0, z0 = 0.9, 1.0, 1.0

# Maximum time point and total number of time points
tmax, n = 1000, 10000

def Project(X, t, sigma, beta, E, Q, omega, gamma, phi, C, D):
    """The model equations"""
    x, y, z = X
    dx = (beta*x*z/(E + z)) - sigma*x
    dy = 1 - (omega*y)
    dz = (gamma*y) - ((1/(C + (D*Q*z)))*((beta*x*z)/(E + z))) - (phi * z)
    return (dx, dy, dz)

# Integrate the model equations on the time grid t.
t = np.linspace(0, tmax, n)
f = odeint(Project, (x0, y0, z0), t, args=(sigma, beta, E, Q, omega, gamma, phi, C, D))
x, y, z = f.T

# Plot the model phase space trajectory using a Matplotlib 3D projection.
fig = plt.figure()
ax = Axes3D(fig)

ax.plot(x, y, z, 'b-', lw=0.5)

ax.set_xlabel('x', fontsize=15)
ax.set_ylabel('y', fontsize=15)
ax.set_zlabel('z', fontsize=15)
plt.tick_params(labelsize=15)
ax.set_title('Model', fontsize=15)

plt.show()

# Plot time series.
fig = plt.figure(figsize=(15, 5))
fig.subplots_adjust(wspace=0.5, hspace=0.3)
ax1 = fig.add_subplot(1, 3, 1)
ax1.set_title('Value of x', fontsize=12)
ax2 = fig.add_subplot(1, 3, 2)
ax2.set_title('Value of y', fontsize=12)
ax3 = fig.add_subplot(1, 3, 3)
ax3.set_title('Value of z', fontsize=12)

ax1.plot(t, x)
ax2.plot(t, y)
ax3.plot(t, z)

plt.show()
print(x)
print(y)
print(z)
