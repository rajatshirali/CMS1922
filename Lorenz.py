# -*- coding: utf-8 -*-
"""
Created on Fri Aug 21 20:50:35 2020

@author: Rajat
"""


# In this case, the odeint numerical solver was used to solve the nonlinear ODEs.

import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Lorenz parameters and initial conditions
sigma, beta, rho = 10, 2.667, 28
x0, y0, z0 = 0, 1, 1.05

# Maximum time point and total number of time points
tmax, n = 100, 10000

def Lorenz(X, t, sigma, beta, rho):
    """The Lorenz equations"""
    x, y, z = X
    dx = -sigma * (x - y)
    dy = rho*x - y - x*z
    dz = -beta*z + x*y
    return (dx, dy, dz)

# Integrate the Lorenz equations on the time grid t.
t = np.linspace(0, tmax, n)
f = odeint(Lorenz, (x0, y0, z0), t, args=(sigma, beta, rho))
x, y, z = f.T

# Plot the Lorenz attractor using a Matplotlib 3D projection.
fig = plt.figure()
ax = Axes3D(fig)

ax.plot(x, y, z, 'b-', lw=0.5)

ax.set_xlabel('x', fontsize=15)
ax.set_ylabel('y', fontsize=15)
ax.set_zlabel('z', fontsize=15)
plt.tick_params(labelsize=15)
ax.set_title('Lorenz Attractor', fontsize=15)

plt.show()

# Plot time series.
fig = plt.figure(figsize=(15, 5))
fig.subplots_adjust(wspace=0.5, hspace=0.3)
ax1 = fig.add_subplot(1, 3, 1)
ax1.set_title('x', fontsize=12)
ax2 = fig.add_subplot(1, 3, 2)
ax2.set_title('y', fontsize=12)
ax3 = fig.add_subplot(1, 3, 3)
ax3.set_title('z', fontsize=12)

ax1.plot(t, x)
ax2.plot(t, y)
ax3.plot(t, z)

plt.show()